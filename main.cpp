#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QLightSensor>
#include <QAmbientLightSensor>
#include <QDebug>
#include <QTimer>

#include "brightnessmanager.h"
#include "weathermodel.h"
#include "radioparadiseinfo.h"

#include <sevensegmentsml.h>
#include <colorpickerqml.h>

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QtAndroid>
#include <QtAndroidExtras/QAndroidJniEnvironment>


#include <QQmlContext>

void keep_screen_on(bool on) {
  QtAndroid::runOnAndroidThread([on]{
	QAndroidJniObject activity = QtAndroid::androidActivity();
	if (activity.isValid()) {
	  QAndroidJniObject window =
		  activity.callObjectMethod("getWindow", "()Landroid/view/Window;");

	  if (window.isValid()) {
		const int FLAG_KEEP_SCREEN_ON = 128;
		if (on) {
		  window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
		} else {
		  window.callMethod<void>("clearFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
		}
	  }
	}
	QAndroidJniEnvironment env;
	if (env->ExceptionCheck()) {
	  env->ExceptionClear();
	}
  });
}
#endif


int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	QGuiApplication app(argc, argv);

	app.setOrganizationName("Ari Bear Inc");
	app.setOrganizationDomain("foobar.com");
	app.setApplicationName("Ari OClock");

	//qmlRegisterType<ColorPickerQML>("pumpkin.controlers", 1, 0, "ColorPicker");
	qmlRegisterType<BrightnessManager>("Bedroom", 1, 0, "Brightness");
	qmlRegisterType<WeatherModel>("Bedroom", 1, 0, "WeatherModel");
	qmlRegisterType<WeatherData>("Bedroom", 1, 0, "WeatherData");
	qmlRegisterType<RadioparadiseInfo>("Bedroom", 1, 0, "RadioParadiseModel");
	qmlRegisterType<SevenSegmentsML>("pumpkin.indicators", 1, 0, "SevenSegments");
	qmlRegisterType<ColorPickerQML>("pumpkin.controlers", 1, 0, "ColorPicker");

#ifdef Q_OS_ANDROID
	keep_screen_on(true);
#endif

	QQmlApplicationEngine engine;
	const QUrl url(QStringLiteral("qrc:/main.qml"));
	QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
					 &app, [url](QObject *obj, const QUrl &objUrl) {
		if (!obj && url == objUrl)
			QCoreApplication::exit(-1);
	}, Qt::QueuedConnection);
	engine.load(url);

	return app.exec();
}
