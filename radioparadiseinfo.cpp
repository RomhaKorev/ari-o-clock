#include "radioparadiseinfo.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTimer>

RadioparadiseInfo::RadioparadiseInfo(QObject *parent) : QObject(parent)
{
	/*timer->setInterval(20000);
	connect(timer, &QTimer::timeout, this, &RadioparadiseInfo::update);

	timer->start();*/
}

void RadioparadiseInfo::update()
{
	QNetworkAccessManager *manager = new QNetworkAccessManager(this);
	connect(manager, &QNetworkAccessManager::finished,
			this, &RadioparadiseInfo::replyFinished);

	connect(manager, &QNetworkAccessManager::finished,
			this, &QNetworkAccessManager::deleteLater);

	manager->get(QNetworkRequest(QUrl("https://api.radioparadise.com/api/get_block?info=true")));
	qDebug()
		<< QSslSocket::supportsSsl() // doit retourner true
		<< QSslSocket::sslLibraryBuildVersionString() // la version utilise pour compiler Qt
		<< QSslSocket::sslLibraryVersionString(); // la version disponible
}


void RadioparadiseInfo::replyFinished(QNetworkReply * reply)
{
	if (reply->error())
	{
		qDebug() << reply->errorString();
		return;
	}
	QByteArray const content = reply->readAll();
	qDebug() << content;
	QJsonDocument document = QJsonDocument::fromJson(content);



	QJsonObject jo = document.object();
	QJsonObject song = jo.value(QStringLiteral("song")).toObject();

	title = song.value("title").toString();
	artist = song.value("artist").toString();
	album = song.value("album").toString();

	int duration = song.value("duration").toInt();

	QTimer::singleShot(duration / 4, this, &RadioparadiseInfo::update);

	qDebug() << title << artist << album;

	emit dataChanged();
}


QString RadioparadiseInfo::getTitle() const
{
	return title;
}

QString RadioparadiseInfo::getArtist() const
{
	return artist;
}

QString RadioparadiseInfo::getAlbum() const
{
	return album;
}
