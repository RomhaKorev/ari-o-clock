#include "brightnessmanager.h"

#include <QDebug>
#include <QTimer>


int BrightnessManager::compute(double lux)
{
	if (lux <= 1)
		return 7;
	if (lux <= 50)
		return 35;
	if (lux <= 300)
		return 50;
	return 100;
}

BrightnessManager::BrightnessManager(QQuickItem *parent) : QQuickItem(parent),
	currentValue(-1), offset(10.0), sensor(new QLightSensor(this)), nightModeTimer(new QTimer(this))
{
	QTimer* timer = new QTimer(this);
	connect(timer, &QTimer::timeout, this, &BrightnessManager::update);
	timer->start(20 * 1000);

	sensor->start();

	nightModeTimer->setInterval(1000 * 10);
	connect(nightModeTimer, &QTimer::timeout, [this](){ emit lightChanged(currentValue / 100.0, 0.075); });
}


void BrightnessManager::update()
{
	auto* value = sensor->reading();
	if (!value)
		return;

	double const lux = value->lux();

	int const actual = compute(lux) * offset;

	if (actual == currentValue)
		return;

	if (actual == 10)
		nightModeTimer->start();
	else
		nightModeTimer->stop();

	emit lightChanged(currentValue / 100.0, actual / 100.0);
	currentValue = actual;
}

double BrightnessManager::getOffset() const
{
	return offset;
}

void BrightnessManager::setOffset(double value)
{
	offset = value;
	update();
}
