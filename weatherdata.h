/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/


#ifndef WEATHERDATA_H
#define WEATHERDATA_H

#include <QObject>
#include <QUrl>
#include <QDateTime>

#include "weathericon.h"

//! [0]
class WeatherData : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString dayOfWeek
			   READ dayOfWeek WRITE setDayOfWeek
			   NOTIFY dataChanged)
	Q_PROPERTY(QString weatherDescription
			   READ weatherDescription WRITE setWeatherDescription
			   NOTIFY dataChanged)
	Q_PROPERTY(QString temperature
			   READ temperature WRITE setTemperature
			   NOTIFY dataChanged)
	Q_PROPERTY(QUrl weatherIcon
			   READ weatherIconUrl
			   NOTIFY dataChanged)
	Q_PROPERTY(QString sunset
			   READ getSunset
			   NOTIFY dataChanged)
	Q_PROPERTY(QString sunrise
			   READ getSunrise
			   NOTIFY dataChanged)
	Q_PROPERTY(double humidity
			   READ getHumidity
			   NOTIFY dataChanged)

public:
	explicit WeatherData(QObject *parent = 0);
	WeatherData(const WeatherData &other);

	QString dayOfWeek() const;
	WeatherIcon weatherIcon() const;
	QString weatherDescription() const;
	QString temperature() const;

	QString getSunset() const;
	QString getSunrise() const;
	double getHumidity() const;

	void setDayOfWeek(const QString &value);
	void setWeatherIcon(const WeatherIcon &value);
	void setWeatherIcon(const QString &value);
	void setWeatherDescription(const QString &value);
	void setTemperature(const QString &value);

	void setSunset(QDateTime const& value);
	void setSunrise(QDateTime const& value);
	void setHumidity(double value);

	QUrl weatherIconUrl() const;

signals:
	void dataChanged();
//! [0]
private:
	QString m_dayOfWeek;
	//QString m_weather;
	WeatherIcon m_weather;
	QString m_weatherDescription;
	QString m_temperature;

	QDateTime sunset;
	QDateTime sunrise;
	double humidity;
//! [1]
};
//! [1]

Q_DECLARE_METATYPE(WeatherData)

#endif // WEATHERDATA_H
