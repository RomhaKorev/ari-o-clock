import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Layouts 1.12
import QtQuick.XmlListModel 2.0

import QtQuick.Controls 2.12

import Bedroom 1.0

Window {
	visible: true
	visibility: "FullScreen"
	//width: 640
	//height: 480
	title: qsTr("Hello World")

	//Component.onCompleted: console.log(Qt.fontFamilies())

	WeatherModel {
		id: model
		city: configuration.currentCity
		onReadyChanged: {
			if (model.ready)
				console.log("Weather ready")
			else
				console.log("Weather loading")
		}
	}

	Brightness {
		onLightChanged: {
			timeOpacityAnimator.from = oldValue
			timeOpacityAnimator.to = newValue
			timeOpacityAnimator.start()
		}
	}

	Rectangle {
		anchors.fill: parent
		color: "#000000"
	}

	StackLayout {
		id: stack
		anchors.fill: parent
		SwipeView {
			currentIndex: 1
			Weather {
				id: weather
				font: configuration.currentFont
				foreground: "white"
				//opacity: clock.opacity
				weather: model.hasValidWeather? model.weather: null
				forecast: model.hasValidWeather? model.forecast: []
			}
			Clock {
				id: clock
				foreground: configuration.color
				OpacityAnimator on opacity {
					id: timeOpacityAnimator
					from: 0;
					to: 1;
					duration: 1000
				}
				onOpacityChanged: console.log(opacity)
				onEdit: stack.currentIndex = 1
			}
			/*RadioParadise {
				font: configuration.currentFont
				foreground: "white"
			}*/
		}
		Configuration {
			id: configuration
			onDone: stack.currentIndex = 0
		}
	}
}
