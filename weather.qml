import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.12

import Bedroom 1.0

Item {
	id: root
	property WeatherData weather: null
	property alias font: temperature.font.family
	property color foreground
	property var forecast: []
	GridLayout {
		anchors.fill: parent
		anchors.margins: 30
		columns: 4
		ColumnLayout {
			Layout.rowSpan: 2
			RowLayout {

				Image {
					id: img
					Layout.alignment: Qt.AlignTop
					source: weather !== null ? weather.weatherIcon: ""
					smooth: true
					Layout.maximumWidth: 120
					Layout.maximumHeight: 120
				}
				Text {
					id: temperature
					Layout.alignment: Qt.AlignTop
					text: (model.hasValidWeather
						   ? model.weather.temperature
						   : "??")
					color: foreground
					font.pointSize: 64
					font.family: "Roboto"
					minimumPointSize: 10
				}
			}
			Text {
				Layout.alignment: Qt.AlignTop
				text: "Humidity"
				color: foreground
				font.pointSize: 18
				font.family: "Roboto"
				minimumPointSize: 10
			}
			Text {
				Layout.alignment: Qt.AlignTop
				text: model.hasValidWeather?model.weather.humidity + "%":"??"
				color: foreground
				font.pointSize: 18
				font.family: "Roboto"
				minimumPointSize: 10
			}
			GridLayout {
				columns: 3
				Text {
					Layout.alignment: Qt.AlignTop
					text: "Sunrise"
					color: foreground
					font.pointSize: 18
					font.family: "Roboto"
					minimumPointSize: 10
				}
				Rectangle {
					width: 2
					Layout.fillHeight: true
					Layout.rowSpan: 2
					Layout.margins: 10
					color: foreground
				}

				Text {
					Layout.alignment: Qt.AlignTop
					text: "Sunset"
					color: foreground
					font.pointSize: 18
					font.family: "Roboto"
					minimumPointSize: 10
				}
				Text {
					Layout.alignment: Qt.AlignTop
					text: model.hasValidWeather?model.weather.sunrise:""
					color: foreground
					font.pointSize: 18
					font.family: "Roboto"
					minimumPointSize: 10
				}
				Text {
					Layout.alignment: Qt.AlignTop
					text: model.hasValidWeather?model.weather.sunset:""
					color: foreground
					font.pointSize: 18
					font.family: "Roboto"
					minimumPointSize: 10
				}
			}
		}
		Repeater {
			model: forecast
			ColumnLayout {
				Layout.maximumWidth: 100
				Layout.maximumHeight: 85
				spacing: 0
				Layout.alignment: Qt.AlignTop | Qt.AlignRight
				Image {
					source: modelData.weatherIcon
					smooth: true
					Layout.alignment: Qt.AlignHCenter
					Layout.preferredWidth: 50
					Layout.preferredHeight: 50
				}
				Text {
					Layout.alignment: Qt.AlignHCenter
					text: modelData.temperature
					color: foreground
					font.pointSize: 30
					font.bold: true
					font.family: root.font
				}
				Text {
					Layout.alignment: Qt.AlignHCenter
					text: modelData.dayOfWeek
					color: foreground
					font.pointSize: 24
					font.bold: true
					font.family: root.font
				}
			}
		}
	}
}
