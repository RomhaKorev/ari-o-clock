import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import pumpkin.indicators 1.0

Item {
	id: root

	property color foreground
	//property alias font: date.font.family

	signal edit()

	Timer {
		id: clockTimer
		interval: 1000;
		running: true;
		repeat: true
		onTriggered: {
			var t = new Date();
			//const sec = parseFloat(Qt.formatTime(t, "ss.zzz"));
			//clockTimer.interval = (60 - sec) * 1000;

			hourUnits.value = parseInt(Qt.formatTime(t, "hh")) % 10;
			minuteUnits.value = parseInt(Qt.formatTime(t, "mm")) % 10;
			minuteTens.value = (parseInt(Qt.formatTime(t, "mm")) - minuteUnits.value) / 10;

			var hourTensValue = (parseInt(Qt.formatTime(t, "hh")) - hourUnits.value) / 10;
			if (hourTensValue === 0)
				hourTens.value = 99;
			else
				hourTens.value = hourTensValue;
		}
	}
	Item {
		anchors.fill: parent
		anchors.margins: 30
		RowLayout {
			anchors.fill: parent
			spacing: 20
			SevenSegments {
				id: hourTens
				Layout.fillHeight: true
				Layout.fillWidth: true
				color: root.foreground
				thickness: 30
			}
			SevenSegments {
				id: hourUnits
				Layout.fillHeight: true
				Layout.fillWidth: true
				color: root.foreground
				thickness: 30
			}
			Item {
				Layout.preferredHeight: parent.height / 3
				Layout.alignment: Qt.AlignCenter
				Layout.preferredWidth: (parent.width / 4) * 0.3
				Rectangle {
					id: rectTop
					anchors.left: parent.left
					anchors.right: parent.right
					anchors.top: parent.top
					height: parent.height / 3
					color: root.foreground
				}
				Rectangle {
					id: rectBottom
					anchors.left: parent.left
					anchors.right: parent.right
					anchors.bottom: parent.bottom
					height: parent.height / 3
					color: root.foreground
				}
			}

			SevenSegments {
				id: minuteTens
				Layout.fillHeight: true
				Layout.fillWidth: true
				color: root.foreground
				thickness: 30
			}
			SevenSegments {
				id: minuteUnits
				Layout.fillHeight: true
				Layout.fillWidth: true
				color: root.foreground
				thickness: 30
			}
		}
	}
	MouseArea {
		anchors.fill: parent
		onPressAndHold: root.edit()
	}

}
