import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import pumpkin.controlers 1.0
import pumpkin.indicators 1.0

import Qt.labs.settings 1.0

Item {
	id: root
	property alias currentFont: font.currentText
	property alias currentCity: city.text
	property alias color: picker.color

	Settings {
		   property alias font: root.currentFont
		   property alias city: root.currentCity
		   property alias color: root.color
	}

	signal done()

	RowLayout {
		anchors.top: parent.top
		anchors.left: parent.left
		anchors.right: parent.right
		anchors.bottom: example.top
		anchors.margins: 20
		ColumnLayout {
			Text {
				text: "City"
				color: "white"
				Layout.alignment: Qt.AlignTop
				Layout.topMargin: 16
			}

			TextField {
				id: city
				text: "montrouge"
				Layout.alignment: Qt.AlignTop
			}
			Text {
				text: "Font"
				color: "white"
				Layout.alignment: Qt.AlignTop
				Layout.topMargin: 16
			}

			ComboBox {
				id: font
				model: Qt.fontFamilies()
				Layout.alignment: Qt.AlignTop
			}

			ColorPicker {
				id: picker
				width: 200
				height: 200
			}

		}
		SevenSegments {
			implicitHeight: root.height - 90
			implicitWidth: implicitHeight / 2.5
			color: picker.color
			value: 8
			thickness: 30
		}
	}

	Button {
		id: example
		text: "Done"
		anchors.bottom: parent.bottom
		anchors.margins: 20
		anchors.horizontalCenter: parent.horizontalCenter
		onClicked: done()
	}
}
