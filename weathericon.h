#ifndef WEATHERICON_H
#define WEATHERICON_H

#include <QString>
#include <QUrl>

enum class Weather
{
	CLEAR = 1,
	FEW_CLOUDS = 2,
	CLOUDS = 3,
	OVERCAST = 4, // couvert
	SHOWERS = 9, // averse
	RAINY = 10,
	THUNDERS_STORM = 11, // pluie d'orage
	SNOW =13,
	MIST = 50,
	UNKNOWN = -1
};

class WeatherIcon
{
public:
	WeatherIcon(QString const& code);
	WeatherIcon();
	WeatherIcon(WeatherIcon const& other);

	bool isValid() const;
	QUrl toUrl() const;

private:
	void defineIconFromCode(QString const& code);
	bool isDay=false;
	Weather weather;
};

#endif // WEATHERICON_H
