/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "weathermodel.h"

#include <qnetworkconfigmanager.h>
#include <qnetworksession.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStringList>
#include <QTimer>
#include <QUrlQuery>
#include <QElapsedTimer>
#include <QLoggingCategory>
#include <QDebug>
/*
 *This application uses http://openweathermap.org/api
 **/

#define ZERO_KELVIN 273.15

Q_LOGGING_CATEGORY(requestsLog,"wapp.requests")



class WeatherModelPrivate
{
public:
	static const int baseMsBeforeNewRequest = 5 * 1000; // 5 s, increased after each missing answer up to 10x
	double longitude, latitude;
	QString city;
	QNetworkAccessManager *nam;
	QNetworkSession *ns;
	WeatherData now;
	QList<WeatherData*> forecast;
	QQmlListProperty<WeatherData> *fcProp;
	bool ready;
	QElapsedTimer throttle;
	int nErrors;
	int minMsBeforeNewRequest;
	QTimer delayedCityRequestTimer;
	QTimer requestNewWeatherTimer;
	QString app_ident;

	WeatherModelPrivate() :
			nam(NULL),
			ns(NULL),
			fcProp(NULL),
			ready(false),
			nErrors(0),
			minMsBeforeNewRequest(baseMsBeforeNewRequest)
	{
		delayedCityRequestTimer.setSingleShot(true);
		delayedCityRequestTimer.setInterval(1000); // 1 s
		requestNewWeatherTimer.setSingleShot(false);
		requestNewWeatherTimer.setInterval(20*60*1000); // 20 min
		throttle.invalidate();
		app_ident = QStringLiteral("36496bad1955bf3365448965a42b9eac");
	}
};

static void forecastAppend(QQmlListProperty<WeatherData> *prop, WeatherData *val)
{
	Q_UNUSED(val);
	Q_UNUSED(prop);
}

static WeatherData *forecastAt(QQmlListProperty<WeatherData> *prop, int index)
{
	WeatherModelPrivate *d = static_cast<WeatherModelPrivate*>(prop->data);
	return d->forecast.at(index);
}

static int forecastCount(QQmlListProperty<WeatherData> *prop)
{
	WeatherModelPrivate *d = static_cast<WeatherModelPrivate*>(prop->data);
	return d->forecast.size();
}

static void forecastClear(QQmlListProperty<WeatherData> *prop)
{
	static_cast<WeatherModelPrivate*>(prop->data)->forecast.clear();
}

//! [0]
WeatherModel::WeatherModel(QObject *parent) :
		QObject(parent),
		d(new WeatherModelPrivate)
{
//! [0]
	d->fcProp = new QQmlListProperty<WeatherData>(this, d,
														  forecastAppend,
														  forecastCount,
														  forecastAt,
														  forecastClear);
	connect(&d->requestNewWeatherTimer, SIGNAL(timeout()),
			this, SLOT(refreshWeather()));
	d->requestNewWeatherTimer.start();


//! [1]
	// make sure we have an active network session
	d->nam = new QNetworkAccessManager(this);

	QNetworkConfigurationManager ncm;
	d->ns = new QNetworkSession(ncm.defaultConfiguration(), this);
	connect(d->ns, SIGNAL(opened()), this, SLOT(networkSessionOpened()));
	// the session may be already open. if it is, run the slot directly
	if (d->ns->isOpen())
		this->networkSessionOpened();
	// tell the system we want network
	d->ns->open();
}
//! [1]

WeatherModel::~WeatherModel()
{
	d->ns->close();
	delete d;
}

//! [2]
void WeatherModel::networkSessionOpened()
{
	d->city = "Montrouge";
	emit cityChanged();
	this->refreshWeather();
}


void WeatherModel::handleGeoNetworkData(QNetworkReply *networkReply)
{
	if (!networkReply) {
		return;
	}

	if (networkReply->error())
	{
		return;
	}
	d->nErrors = 0;
	if (!d->throttle.isValid())
		d->throttle.start();
	d->minMsBeforeNewRequest = d->baseMsBeforeNewRequest;
	//convert coordinates to city name
	QJsonDocument document = QJsonDocument::fromJson(networkReply->readAll());

	QJsonObject jo = document.object();
	QJsonValue jv = jo.value(QStringLiteral("name"));

	const QString city = jv.toString();
	//qCDebug(requestsLog) << "got city: " << city << jo;
	if (city != d->city) {
		d->city = city;
		emit cityChanged();
		refreshWeather();
	}

	networkReply->deleteLater();
}

void WeatherModel::refreshWeather()
{
	if (d->city.isEmpty()) {
		qCDebug(requestsLog) << "refreshing weather skipped (no city)";
		return;
	}
	qCDebug(requestsLog) << "refreshing weather";
	QUrl url("http://api.openweathermap.org/data/2.5/weather");
	QUrlQuery query;

	query.addQueryItem("q", d->city);
	query.addQueryItem("mode", "json");
	query.addQueryItem("APPID", d->app_ident);
	url.setQuery(query);

	QNetworkReply *rep = d->nam->get(QNetworkRequest(url));
	// connect up the signal right away
	connect(rep, &QNetworkReply::finished,
			this, [this, rep]() { handleWeatherNetworkData(rep); });
}

static QString niceTemperatureString(double t)
{
	return QString::number(qRound(t-ZERO_KELVIN)) + QChar(0xB0);
}

void WeatherModel::handleWeatherNetworkData(QNetworkReply *networkReply)
{
	if (!networkReply)
		return;

	if (!networkReply->error()) {
		foreach (WeatherData *inf, d->forecast)
			delete inf;
		d->forecast.clear();

		QJsonDocument document = QJsonDocument::fromJson(networkReply->readAll());

//		qDebug() << document;

		if (document.isObject()) {
			QJsonObject obj = document.object();
			QJsonObject tempObject;
			QJsonValue val;

			if (obj.contains(QStringLiteral("coord"))) {
				QJsonObject coord = obj.value("coord").toObject();
				d->latitude = coord.value("lat").toDouble();
				d->longitude = coord.value("lon").toDouble();
			}

			if (obj.contains(QStringLiteral("weather"))) {
				val = obj.value(QStringLiteral("weather"));
				QJsonArray weatherArray = val.toArray();
				val = weatherArray.at(0);
				tempObject = val.toObject();
				d->now.setWeatherDescription(tempObject.value(QStringLiteral("description")).toString());
				d->now.setWeatherIcon(tempObject.value("icon").toString());
			}
			if (obj.contains(QStringLiteral("main"))) {
				val = obj.value(QStringLiteral("main"));
				tempObject = val.toObject();
				val = tempObject.value(QStringLiteral("temp"));
				d->now.setTemperature(niceTemperatureString(val.toDouble()));
				d->now.setHumidity(tempObject.value(QStringLiteral("humidity")).toDouble());
			}
			if (obj.contains(QStringLiteral("sys"))) {
				val = obj.value(QStringLiteral("sys"));
				tempObject = val.toObject();
				d->now.setSunset(QDateTime::fromSecsSinceEpoch(tempObject.value(QStringLiteral("sunset")).toInt()));
				d->now.setSunrise(QDateTime::fromSecsSinceEpoch(tempObject.value(QStringLiteral("sunrise")).toInt()));
			}
		}
	}
	networkReply->deleteLater();

	//retrieve the forecast
	//QUrl url("http://api.openweathermap.org/data/2.5/forecast/daily");
	QUrl url("http://api.openweathermap.org/data/2.5/onecall");
	QUrlQuery query;

	//qDebug() << d->latitude << d->longitude;
	query.addQueryItem("lat", QString::number(d->latitude));
	query.addQueryItem("lon", QString::number(d->longitude));
	query.addQueryItem("APPID", d->app_ident);
	query.addQueryItem("exclude", "current,minutely,daily");
	url.setQuery(query);

	QNetworkReply *rep = d->nam->get(QNetworkRequest(url));
	// connect up the signal right away
	connect(rep, &QNetworkReply::finished,
			this, [this, rep]() { handleForecastNetworkData(rep); });
}

void WeatherModel::handleForecastNetworkData(QNetworkReply *networkReply)
{
	if (!networkReply)
		return;

	//qDebug() << networkReply->errorString();

	if (!networkReply->error()) {
		QJsonDocument document = QJsonDocument::fromJson(networkReply->readAll());

		//qDebug() << document;

		d->forecast.clear();

		QJsonObject jo;
		QJsonValue jv;
		QJsonObject root = document.object();
		jv = root.value(QStringLiteral("hourly"));
		if (!jv.isArray())
			qWarning() << "Invalid forecast object";
		QJsonArray ja = jv.toArray();
		if (ja.count() != 48)
			qWarning() << "Invalid forecast object";

		QList<int> highlightedHours;
		highlightedHours << 8 << 10 << 12 << 14 << 18 << 22;
		QString data;
		for (int i = 1; i<ja.count(); i++) {
			QJsonObject subtree = ja.at(i).toObject();

			jv = subtree.value(QStringLiteral("dt"));
			QDateTime dt = QDateTime::fromMSecsSinceEpoch((qint64)jv.toDouble()*1000);

			if (!highlightedHours.contains(dt.time().hour()))
				continue;
			highlightedHours.removeOne(dt.time().hour());

			WeatherData *forecastEntry = new WeatherData();
			forecastEntry->setDayOfWeek(dt.toString("hh:00"));

			jv = subtree.value(QStringLiteral("temp"));
			data.clear();
			data += niceTemperatureString(jv.toDouble());
			forecastEntry->setTemperature(data);

			//get icon
			QJsonArray weatherArray = subtree.value(QStringLiteral("weather")).toArray();
			jo = weatherArray.at(0).toObject();
			forecastEntry->setWeatherIcon(jo.value(QStringLiteral("icon")).toString());

			//get description
			forecastEntry->setWeatherDescription(jo.value(QStringLiteral("description")).toString());

			d->forecast.append(forecastEntry);
			if (d->forecast.size() == 6)
				break;
		}

		if (!(d->ready)) {
			d->ready = true;
			emit readyChanged();
		}

		emit weatherChanged();
	}
	networkReply->deleteLater();
}

bool WeatherModel::hasValidCity() const
{
	return (!(d->city.isEmpty()) && d->city.size() > 1 && d->city != "");
}

bool WeatherModel::hasValidWeather() const
{
	return hasValidCity() && d->now.weatherIcon().isValid();
}

WeatherData *WeatherModel::weather() const
{
	return &(d->now);
}

QQmlListProperty<WeatherData> WeatherModel::forecast() const
{
	return *(d->fcProp);
}

bool WeatherModel::ready() const
{
	return d->ready;
}

QString WeatherModel::city() const
{
	return d->city;
}

void WeatherModel::setCity(const QString &value)
{
	d->city = value;
	emit cityChanged();
	refreshWeather();
}
