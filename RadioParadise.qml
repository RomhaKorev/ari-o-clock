import QtQuick 2.0
import QtQuick.Layouts 1.12

import Bedroom 1.0


Item {
	id: root
	property alias font: title.font.family
	property color foreground

	//onVisibleChanged: info.update();
	onFocusChanged:  info.update();

	RadioParadiseModel {
		id: info;
	}

	GridLayout {
		anchors.fill: parent;
		anchors.margins: 30
		columns: 2
		Text {
			text: "Title:"
			font: title.font
			color: foreground
		}
		Text {
			id: title
			text: info.title
			Layout.fillWidth: true
			font: title.font
			color: foreground
		}
		Text {
			text: "Artist:"
			font: title.font
			color: foreground
		}
		Text {
			id: artist
			text: info.artist
			Layout.fillWidth: true

			font: title.font
			color: foreground
		}
		Text {
			text: "album:"
			font: title.font
			color: foreground
		}
		Text {
			id: album
			text: info.album
			Layout.fillWidth: true

			font: title.font
			color: foreground
		}
	}
}
