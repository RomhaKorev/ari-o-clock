QT += quick sensors network
CONFIG += c++11

TARGET = AriOclock


android  {
    QT += androidextras
}

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
		brightnessmanager.cpp \
		main.cpp \
		radioparadiseinfo.cpp \
		weatherdata.cpp \
		weathericon.cpp \
		weathermodel.cpp

RESOURCES += qml.qrc \
    resources.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
	brightnessmanager.h \
	radioparadiseinfo.h \
	weatherdata.h \
	weathericon.h \
	weathermodel.h

DISTFILES += \
    android/AndroidManifest.xml \
	android/build.gradle \
	android/gradle/wrapper/gradle-wrapper.jar \
	android/gradle/wrapper/gradle-wrapper.properties \
	android/gradlew \
	android/gradlew.bat \
	android/res/values/libs.xml


#INCLUDEPATH += $$PWD/../Pumpkin
#DEPENDPATH += $$PWD/../Pumpkin

#!android {
#    LIBS += -L$$PWD/../build/Pumpkin-Desktop/controlers/qml/debug -lPumpkinControlers
#}

#android {
#    LIBS += -L$$PWD/../build/Pumpkin-Android/controlers/qml/debug -lPumpkinControlers
#}


INCLUDEPATH += $$PWD/../Pumpkin/indicators/SevenSegments
DEPENDPATH += $$PWD/../Pumpkin/indicators/SevenSegments
INCLUDEPATH += $$PWD/../Pumpkin/controlers/colorpicker/qml
DEPENDPATH += $$PWD/../Pumpkin/controlers/colorpicker/qml

unix:!macx: LIBS += -L$$PWD/../build/Pumpkin-Android/indicators/sevensegments/ -lPumpkinSevenSegments
unix:!macx: PRE_TARGETDEPS += $$PWD/../build/Pumpkin-Android/indicators/sevensegments/libPumpkinSevenSegments.a
unix:!macx: LIBS += -L$$PWD/../build/Pumpkin-Android/controlers/colorpicker/ -lPumpkinControlers



unix:!macx: PRE_TARGETDEPS += $$PWD/../build/Pumpkin-Android/controlers/colorpicker/libPumpkinControlers.a
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build/Pumpkin-Desktop/controlers/colorpicker/release/ -lPumpkinControlers
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build/Pumpkin-Desktop/controlers/colorpicker/debug/ -lPumpkinControlers

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build/Pumpkin-Desktop/indicators/sevensegments/release/ -lPumpkinSevenSegments
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build/Pumpkin-Desktop/indicators/sevensegments/debug/ -lPumpkinSevenSegments

#LIBS += -LC:/Qt/Tools/OpenSSL/Win_x64/lib -llibcrypto -llibssl

android: include(<path/to/android_openssl/openssl.pri)
