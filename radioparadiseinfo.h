#ifndef RADIOPARADISEINFO_H
#define RADIOPARADISEINFO_H

#include <QObject>

class QTimer;
class QNetworkReply;

class RadioparadiseInfo : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString title READ getTitle NOTIFY dataChanged);
	Q_PROPERTY(QString artist READ getArtist NOTIFY dataChanged);
	Q_PROPERTY(QString album READ getAlbum NOTIFY dataChanged);
public:
	explicit RadioparadiseInfo(QObject *parent = nullptr);

	Q_INVOKABLE void update();

	Q_INVOKABLE QString getTitle() const;
	Q_INVOKABLE QString getArtist() const;
	Q_INVOKABLE QString getAlbum() const;
signals:
	void dataChanged();
private:
	void replyFinished(QNetworkReply *);
	QString artist;
	QString album;
	QString title;

};

#endif // RADIOPARADISEINFO_H
