#include "weathericon.h"

#include <QTimer>

WeatherIcon::WeatherIcon():
	isDay(true), weather(Weather::UNKNOWN)
{
}

WeatherIcon::WeatherIcon(QString const& code):
	WeatherIcon()
{
	defineIconFromCode(code);
}


WeatherIcon::WeatherIcon(WeatherIcon const& other)
{
	this->isDay = other.isDay;
	this->weather = other.weather;
}

void WeatherIcon::defineIconFromCode(QString const& code)
{
	bool ok = false;
	int const index = code.mid(0, 2).toInt(&ok);

	if (!ok)
		return;

	isDay = code.endsWith("d");
	weather = static_cast<Weather>(index);
}


bool WeatherIcon::isValid() const
{
	return weather != Weather::UNKNOWN;
}

QUrl WeatherIcon::toUrl() const
{
	QString const suffix = isDay ? "day": "night";
	switch(weather)
	{
	case Weather::CLEAR:
		return QUrl("/img/clear_" + suffix);
	case Weather::FEW_CLOUDS:
		return QUrl("/img/few_clouds_" + suffix);
	case Weather::CLOUDS:
		return QUrl("/img/cloudy_" + suffix);
	case Weather::OVERCAST:
		return QUrl("/img/overcast_" + suffix);
	case Weather::SHOWERS:
		return QUrl("/img/showers_" + suffix);
	case Weather::RAINY:
		return QUrl("/img/rainy_" + suffix);
	case Weather::THUNDERS_STORM:
		return QUrl("/img/thunderstorm_" + suffix);
	case Weather::SNOW:
		return QUrl("/img/snow_" + suffix);
	case Weather::MIST:
		return QUrl("/img/fog_" + suffix);
	default:
		return QUrl();
	}
}
