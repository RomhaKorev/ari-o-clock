#ifndef LIGHTSENSOR_H
#define LIGHTSENSOR_H

#include <QQuickItem>
#include <QLightSensor>

class BrightnessManager : public QQuickItem
{
	Q_OBJECT
	Q_PROPERTY(double offset READ getOffset WRITE setOffset);
public:
	explicit BrightnessManager(QQuickItem *parent = nullptr);
	Q_INVOKABLE double getOffset() const;
	Q_INVOKABLE void setOffset(double value);
signals:
	void lightChanged(double oldValue, double newValue);
public slots:
	void update();

private:
	int currentValue;
	double offset;
	QLightSensor* sensor;
	static int compute(double lux);
	QTimer* nightModeTimer;
};

#endif // LIGHTSENSOR_H
